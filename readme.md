This is an example for the supply chain security analysis tool [Macaron](https://github.com/oracle-samples/macaron). The example provides a configuration model for configuration and code generation. It is based on [Epsilon](https://www.eclipse.org/epsilon).

It is intended to show the applicability of MDE/DevOps to Software Engineering Research and prototyping.

https://blogs.oracle.com/developers/post/macaron-supply-chain-conformance-verifier-and-policy-engine-for-slsa


https://oracle.github.io/macaron/pages/installation.html#download

`tag=v0.1.1.`

```sh
curl -O https://raw.githubusercontent.com/oracle/macaron/<tag>/scripts/release_scripts/run_macaron.sh
chmod +x run_macaron.sh
```
