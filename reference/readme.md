https://cuelang.org/


Behnaz Hassanshahi <behnaz.hassanshahi@oracle.com>
Attachments
24 May 2023, 11:37
to me

Hi JG,

Please find attached an example Datalog and CUE policies for Macaron.

You can find the prelude.dl​file included in the Datalog file here: https://github.com/oracle-samples/macaron/blob/907eedf543131a2dd8d406bfa3ad54b3df8fae9a/src/macaron/policy_engine/prelude/prelude.dl

We have also published a blog post today that might be helpful to understand the role of policies: https://blogs.oracle.com/developers/post/macaron-supply-chain-conformance-verifier-and-policy-engine-for-slsa

Please let me know if you have any questions.

Regards,
Behnaz